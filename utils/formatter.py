from datetime import datetime

from tzlocal import get_localzone


class TextFormatterManager:

    @staticmethod
    def format_last(last):
        date = datetime.strptime(last[0][:-4], '%Y-%m-%dT%H:%M:%S.%f')
        date_local = get_localzone().fromutc(date)
        return """
Дата: {0}
Температура: {1}°C,
Влажность: {2}%
        """.format(date_local.strftime('%Y-%m-%d %H:%M:%S'), last[1], last[2])

    @staticmethod
    def format_period(data):
        # 2018-10-21T12:38:50.167440554Z
        date_start = datetime.strptime(data[0][0][:-4], '%Y-%m-%dT%H:%M')
        date_end = datetime.strptime(data[len(data) - 1][0][:-4], '%Y-%m-%dT%H:%M')
        date_start_local = get_localzone().fromutc(date_start)
        date_end_local = get_localzone().fromutc(date_end)
        temperature = 0
        humidity = 0
        for item in data:
            temperature += float(item[1])
            humidity += float(item[2])
        str_out = """
C: {0},
По: {1}
Средняя температура: {2}°C,
Средняя влажность: {3}%
                """.format(date_start_local.strftime('%Y-%m-%d %H:%M:%S'),
                           date_end_local.strftime('%Y-%m-%d %H:%M:%S'),
                           str(round(temperature / len(data), 2)),
                           str(round(humidity / len(data), 2)))

        return str_out
