import io
import logging
from datetime import datetime, timedelta, date
from logging.handlers import RotatingFileHandler
from random import randint

import matplotlib.pyplot as plt
import numpy as np
import telegram as tg
import tzlocal
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from tzlocal import get_localzone

from .formatter import TextFormatterManager
from .requester import RequesterManager

log_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger()
fh = logging.handlers.RotatingFileHandler('log/bot.log', maxBytes=1000000, backupCount=20)
fh.setLevel(logging.DEBUG)
fh.setFormatter(log_format)
logger.addHandler(fh)


class BoardError(Exception):
    pass


class MicroclimateBot:
    STR_START = 'Привет, пользователь 🤠! Воспользуйся командой /help, что бы узнать, как работать со мной.'
    STR_HELP = """
    Даный бот предназначен для просмотра даных о показаниях 
с системы JediengMicroclimate, если она у вас установлена.
Он поддерживает следующий набор команд:
/start - инициировать работу с ботом;
/help - вызвать справку;
/board <серийный_номер_платы> - установить для пользователя серийный номер МК для получения данных.
Спасибо.
    """
    STR_ERROR = 'Простите, возникла ошибка.'
    STR_SET_BOARD_ARGS_ERROR = 'Для установки серийного номера платы нужно ' \
                               'использовать команду /board <номер_платы> ' \
                               '(без знаков "<>").'
    STR_SET_BOARD_COMPLETE = 'Номер платы {0} установлен для пользователя {1}.'

    STR_1H = 'За 1 час 🕐🌡'
    STR_1day = 'За 1 день 🌇🌡'
    STR_LAST = 'Последние показания 🌡'
    STR_7d = 'За неделю 🗒🌡'
    STR_ALL_TIME = 'За всё время 🗓🌡'
    STR_HEATMAP = 'Температурная карта за сутки 🗺️'

    STR_SEND_WADDLES = 'Отправить Пухлю 🐷'

    WADDLES_DICT = {
        0: 'CAADAgADAQADFgqsEHouQiu0Tr5WAg',
        1: 'CAADAgADAgADFgqsEJRvZOvAM3yoAg',
        2: 'CAADAgADAwADFgqsEDRFySGnGRr0Ag',
        3: 'CAADAgADBwADFgqsEA7M9oZNeed8Ag',
        4: 'CAADAgADCAADFgqsEPkMzaS3xIJ1Ag',
        5: 'CAADAgADBAADFgqsEAMWYnVwe4yRAg',
        6: 'CAADAgADBgADFgqsENgXm4B',
        7: 'CAADAgADCQADFgqsEA1q1XcNzEx6Ag',
        8: 'CAADAgADDgADFgqsEEp77Z5AY7hrAg',
        9: 'CAADAgADDAADFgqsEAlxNXs6FBgOAg',
        10: 'CAADAgADBQADFgqsEFDrIa1oyAXjAg',
        11: 'CAADAgADCwADFgqsEBYicUwy7xj8Ag',
        12: 'CAADAgADDQADFgqsEBPksgi8lxcaAg',
        13: 'CAADAgADDwADFgqsEJ72zfA3jJaNAg',
        14: 'CAADAgADNQADFgqsED9uwFa0-kjHAg',
        15: 'CAADAgADEQADFgqsEG1HufYsO4V5Ag',
        17: 'CAADAgADEwADFgqsEMhF0PDtQIthAg',
        18: 'CAADAgADOAADFgqsEBrAm6-PGfZJAg',
        19: 'CAADAgADNAADFgqsEEho5Sq0nJ9JAg',
        20: 'CAADAgADEAADFgqsELpgOnM_DpG7Ag',
        21: 'CAADAgADFAADFgqsEE0w5rjiH6pzAg',
        22: 'CAADAgADEgADFgqsEEc2B1PrCMqIAg',
        23: 'CAADAgADFQADFgqsEFLrgRpOhlphAg',
        24: 'CAADAgADNgADFgqsED13E5ZSmHH4Ag',
        25: 'CAADAgADNwADFgqsEIRwisCKMZMiAg'
    }

    requester = RequesterManager('YXJkdWlubzpZVGRrWVdNNE1EQm1Nak5pTnpoaA==', 'arduino')

    def __init__(self, token):
        self.token = token

    @staticmethod
    def get_keyboard():
        keyboard = [[tg.KeyboardButton(text=MicroclimateBot.STR_SEND_WADDLES)],
                    [tg.KeyboardButton(text=MicroclimateBot.STR_LAST),
                     tg.KeyboardButton(text=MicroclimateBot.STR_1H),
                     tg.KeyboardButton(text=MicroclimateBot.STR_1day)],
                    [tg.KeyboardButton(text=MicroclimateBot.STR_7d),
                     tg.KeyboardButton(text=MicroclimateBot.STR_ALL_TIME)],
                    [tg.KeyboardButton(text=MicroclimateBot.STR_HEATMAP)]]
        return tg.ReplyKeyboardMarkup(keyboard, resize_keyboard=True)

    @staticmethod
    def start(bot, update):
        bot.send_sticker(update.message.chat.id, 'CAADAgADNwADFgqsEIRwisCKMZMiAg')
        bot.send_message(update.message.chat.id, MicroclimateBot.STR_START,
                         reply_markup=MicroclimateBot.get_keyboard())

    @staticmethod
    def help(bot, update):
        chat_id = update.message.chat.id
        bot.send_message(chat_id, MicroclimateBot.STR_HELP)

    @staticmethod
    def set_board_id(bot, update, args, user_data):
        if len(args) != 1:
            bot.send_message(update.message.chat.id, MicroclimateBot.STR_SET_BOARD_ARGS_ERROR)
        else:
            user_data['board'] = args[0]
            bot.send_message(update.message.chat.id, MicroclimateBot.STR_SET_BOARD_COMPLETE.format(
                args[0], update.message.from_user.username
            ))
            bot.send_sticker(update.message.chat.id, 'CAADAgADBgADFgqsENgXm4B-tTIBAg')

    @staticmethod
    def error(bot, update, error):
        try:
            logger.error((str(error)))
        finally:
            if update is not None:
                chat_id = update.message.chat.id
                bot.send_message(chat_id, MicroclimateBot.STR_ERROR)
                bot.send_sticker(update.message.chat.id, 'CAADAgADBwADFgqsEA7M9oZNeed8Ag')

    @staticmethod
    def process_text(bot, update, user_data):
        text = update.message.text
        if text == MicroclimateBot.STR_SEND_WADDLES:
            waddles_ind = randint(0, 25)
            bot.send_sticker(update.message.chat.id, MicroclimateBot.WADDLES_DICT[waddles_ind])
            return

        if 'board' not in user_data:
            bot.send_message(update.message.chat.id, MicroclimateBot.STR_SET_BOARD_ARGS_ERROR)
            raise BoardError('Board not defined for user {}'.format(update.message.from_user.username))

        if text == MicroclimateBot.STR_1H:
            data_1h = MicroclimateBot.requester.get_1h(user_data['board'])
            result = MicroclimateBot.process_data_from_db(data_1h)
            bot.send_photo(update.message.chat.id, result[1])
            bot.send_message(update.message.chat.id, result[0])

        elif text == MicroclimateBot.STR_1day:
            data_1day = MicroclimateBot.requester.get_1day(user_data['board'])
            result = MicroclimateBot.process_data_from_db(data_1day)
            bot.send_photo(update.message.chat.id, result[1])
            bot.send_message(update.message.chat.id, result[0])

        elif text == MicroclimateBot.STR_LAST:
            last = MicroclimateBot.requester.get_last(user_data['board'])
            last_text = TextFormatterManager.format_last(last[0])
            bot.send_message(update.message.chat.id, last_text)

        elif text == MicroclimateBot.STR_7d:
            data_7d = MicroclimateBot.requester.get_7d(user_data['board'])
            result = MicroclimateBot.process_data_from_db(data_7d)
            bot.send_photo(update.message.chat.id, result[1])
            bot.send_message(update.message.chat.id, result[0])

        elif text == MicroclimateBot.STR_ALL_TIME:
            data_all = MicroclimateBot.requester.get_all(user_data['board'])
            result = MicroclimateBot.process_data_from_db(data_all)
            bot.send_photo(update.message.chat.id, result[1])
            bot.send_message(update.message.chat.id, result[0])

        elif text == MicroclimateBot.STR_HEATMAP:
            data_1d = MicroclimateBot.requester.get_1d(user_data['board'])
            heatmap = MicroclimateBot.get_heatmap_for_1d(data_1d)
            bot.send_photo(update.message.chat.id, heatmap)

    @staticmethod
    def process_data_from_db(data):
        data = [item for item in data if item[1] is not None and item[2] is not None]
        text = TextFormatterManager.format_period(data)
        humidity = [float(item[2]) for item in data]
        temperature = [float(item[1]) for item in data]
        x = range(0, len(data))
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.plot(x, humidity, 'g-')
        ax2.plot(x, temperature, 'b-')
        ax1.set_xlabel('Date')
        ax1.set_ylabel('Humidity, %', color='g')
        ax2.set_ylabel('Temperature, °C', color='b')
        ind1 = 0
        ind2 = round(len(x) / 3)
        ind3 = round(len(x) / 1.5)
        ind4 = len(x) - 1
        plt.xticks(
            (ind1, ind2, ind3, ind4),
            (get_localzone().fromutc(datetime.strptime(data[ind1][0][:-4],
                                                       '%Y-%m-%dT%H:%M')).strftime('%Y-%m-%d %H:%M'),
             get_localzone().fromutc(datetime.strptime(data[ind2][0][:-4],
                                                       '%Y-%m-%dT%H:%M')).strftime('%Y-%m-%d %H:%M'),
             get_localzone().fromutc(datetime.strptime(data[ind3][0][:-4],
                                                       '%Y-%m-%dT%H:%M')).strftime('%Y-%m-%d %H:%M'),
             get_localzone().fromutc(datetime.strptime(data[ind4][0][:-4],
                                                       '%Y-%m-%dT%H:%M')).strftime('%Y-%m-%d %H:%M'))
        )
        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plt.close(fig)
        return text, buf

    @staticmethod
    def get_heatmap_for_1d(data):
        heatmap = np.zeros((24, 30))

        for item in data:
            if item[1] is None:
                continue
            cdate = datetime.strptime(item[0][:-4], '%Y-%m-%dT%H:%M')
            cdate += tzlocal.get_localzone().utcoffset(datetime.now())
            heatmap[cdate.hour, int(cdate.minute / 2)] += item[1] / 2

        fig = plt.figure()
        ax = fig.add_subplot(111)
        im = ax.imshow(heatmap, interpolation='nearest')
        ax.set_yticks(range(1, 25))
        ax.set_yticklabels(range(1, 25))
        minutes = np.array(range(0, 31, 2))
        ax.set_xticks(minutes)
        ax.set_xticklabels(['{}'.format(m * 2) for m in minutes])
        ax.set_xlabel('Минуты')
        ax.set_ylabel('Часы')
        prev_date = date.today() - timedelta(1)
        prev_datetime = datetime(prev_date.year, prev_date.month, prev_date.day)
        # prev_datetime -= tzlocal.get_localzone().utcoffset(datetime.now())
        prev_datetime = prev_datetime.strftime('%Y-%m-%d')
        ax.set_title('Температура за %s, °C' % prev_datetime)

        # Add a colour bar along the bottom and label it
        cbar = fig.colorbar(ax=ax, mappable=im, orientation='horizontal')
        cbar.set_label('Температуры, °C')

        buf = io.BytesIO()
        plt.savefig(buf, format='png')
        buf.seek(0)
        plt.close(fig)
        return buf

    def run(self):
        updater = Updater(self.token)

        dp = updater.dispatcher

        dp.add_handler(CommandHandler("start", MicroclimateBot.start))
        dp.add_handler(CommandHandler("help", MicroclimateBot.help))
        dp.add_handler(CommandHandler("board", MicroclimateBot.set_board_id, pass_user_data=True, pass_args=True))
        dp.add_handler(MessageHandler(Filters.text, MicroclimateBot.process_text, pass_user_data=True))
        dp.add_error_handler(MicroclimateBot.error)
        updater.start_polling()
        updater.idle()
