import datetime

import requests
import tzlocal


class RequesterManager:

    def __init__(self, credentials, db):
        self.db = db
        self.auth_header = {'Authorization': 'Basic {}'.format(credentials)}

    def __get_data_base(self, board, time=None, last=False):
        if last:
            query = "SELECT time, LAST(temperature), humidity FROM microclimate WHERE id = '{}'".format(board)
        else:
            query = "SELECT time, mean(temperature), mean(humidity) FROM microclimate WHERE id = '{}'".format(board)
            if time is not None:
                if time in ('1h', '7d', '30m', '24h'):
                    query += ' and time > now() - {}'.format(time)
                elif time in '1d':
                    prev_date = datetime.date.today() - datetime.timedelta(1)
                    prev_datetime = datetime.datetime(prev_date.year, prev_date.month, prev_date.day)
                    prev_datetime -= tzlocal.get_localzone().utcoffset(datetime.datetime.now())
                    prev_datetime = prev_datetime.strftime('%Y-%m-%dT%H:%M:%SZ')
                    query += " and time > '{}' and time < '{}' + 1d".format(prev_datetime, prev_datetime)
            query += " GROUP BY time(1m) FILL(linear)"

        params = {'db': self.db, 'q': query}
        r = requests.get('http://jedieng.com:8086/query?pretty=true', headers=self.auth_header, params=params)

        json_data = r.json()
        values = json_data['results'][0]['series'][0]['values']
        return values

    def get_1day(self, board):
        return self.__get_data_base(board, '24h')

    def get_1h(self, board):
        return self.__get_data_base(board, '1h')

    def get_7d(self, board):
        return self.__get_data_base(board, '7d')

    def get_1d(self, board):
        return self.__get_data_base(board, '1d')

    def get_all(self, board):
        return self.__get_data_base(board)

    def get_last(self, board):
        return self.__get_data_base(board, last=True)
